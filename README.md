# GPP Decrypt
Decrypt group policy preferences password on the go.

### Usage
```
ruby gpp-decrypt.rb [ENCRYPTED DATA]
```